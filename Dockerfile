FROM alpine:3.15

ARG ANSIBLE_VERSION_ARG "2.12.1"
ENV ANSIBLE_VERSION ${ANSIBLE_VERSION_ARG}

ARG user=jenkins
ARG group=jenkins
ARG uid=1027
ARG gid=1027
ARG PYTHON_PIP_VERSION=21.1.1

ENV LANG='en_US.UTF-8' \
    LANGUAGE='en_US:en' \
    LC_ALL='en_US.UTF-8' \
    TZ="Europe/Berlin" \
    ANSIBLE_HOST_KEY_CHECKING=False \
    ANSIBLE_LOCAL_TEMP=/var/tmp/.ansible/tmp \
    ANSIBLE_PERSISTENT_CONTROL_PATH_DIR=/var/tmp/.ansible/pc \
    # ANSIBLE_COLLECTIONS_PATHS=/vol1/ansible \
    # ANSIBLE_ROLES_PATH=/vol1/ansible \
    ANSIBLE_CACHE_PLUGIN_CONNECTION=$HOME

USER root
WORKDIR /root
COPY requirements.txt /root/

RUN apk --no-cache add \
        sudo \
        python3\
        py3-pip \
        openssl \
        ca-certificates \
        sshpass \
        openssh-client \
        rsync \
        git && \

    apk --no-cache add --virtual build-dependencies \
        python3-dev \
        libffi-dev \
        musl-dev \
        gcc \
        cargo \
        openssl-dev \
        libressl-dev \
        build-base && \
    pip3 install ---no-cache-dir --upgrade "pip>=${PYTHON_PIP_VERSION}" \
    pip3 install --no-cache-dir ansible==${ANSIBLE_VERSION} &&\
    apk del build-dependencies && \
    rm -rf /var/cache/apk/* && \
    rm -rf /root/.cache/pip && \
    rm -rf /root/.cargo
    #    alternatives --set python /usr/bin/python3
USER ${user}
WORKDIR /home/${user}
SHELL ["/bin/bash", "-c"]


CMD ["ansible-playbook"]
